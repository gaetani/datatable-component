// @flow
import * as React from 'react';


export default class HolderScroll extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      firstColumnPosition: {
        left: '0',
      },
      headerPosition: {
        top: `0`,
      }
    };
  }

  handleScroll = (event) => {

    const left = event.currentTarget.scrollX;
    const top = event.currentTarget.scrollY;

    this.setState({
      firstColumnPosition:{
        left: `${left}px`,
      },
      headerPosition: {
        top: `${top}px`,
      }
    });
}

    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.handleScroll);
    } 

  

  render() {
    return this.props.children({
        firstColumnPosition: this.state.firstColumnPosition,
        headerPosition: this.state.headerPosition
      });
  }
}

