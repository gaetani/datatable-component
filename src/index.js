import DataTable from './DataTable';
import DataTableHeader from './DataTableHeader';
import DataTableItem from './DataTableItem';
import HolderScroll from './HolderScroll';

export default DataTable;
export {DataTable, DataTableHeader, DataTableItem, HolderScroll};