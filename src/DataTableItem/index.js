// @flow
import * as React from 'react';
import PropTypes from 'prop-types';

export default class DataTableItem extends React.Component {
  constructor(props) {
    super(props);
  }

  check = () => {
    this.props.callback(this.props.index, !this.props.checked);
  }

  render() {
    
    return (
        
            <tr className="active">
              <th style={this.props.firstColumnPosition}><input type="checkbox" checked={this.props.checked} onChange={this.check} /></th>
              {this.props.columns.map(column => <td key={column.attribute}>{''+this.props.data[column.attribute]}</td>)}
            </tr>
        
    )
  }
}

