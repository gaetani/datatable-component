// @flow
import * as React from 'react';
import PropTypes from 'prop-types';

export default class DataTableHeader extends React.Component {
  constructor(props) {
    super(props);
  }


  render() {
    const spaceXY = {...this.props.firstColumnPosition, ... this.props.headerPosition};
    
    return (
      <thead >
          <tr >
            <th style={spaceXY}>
                <input type="checkbox" checked={this.props.checked} onChange={this.props.callback} />
            </th>
            { this.props.columns.map(colum => 
              <th style={this.props.headerPosition} key={colum.attribute}>{colum.label}</th>
            )}
          </tr>
      </thead>
    )
  }
}

