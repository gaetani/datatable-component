// @flow
import * as React from 'react';
import PropTypes from 'prop-types';
import DataTableHeader from '../DataTableHeader';
import DataTableItem from '../DataTableItem';
import './DataTable.css';
import HolderScroll from '../HolderScroll';

export default class DataTable extends React.Component {

  constructor(props) {
    super(props);
    const checkItemState = props.data.map(() => false);
    this.state = {
      checkItemState: checkItemState,
      allCheck: false,
    };
  }

  checkRow = (id,value) => {
    const checkItemState = this.state.checkItemState;  
    checkItemState[id] = value;
    const allCheck = checkItemState.reduce((ant, next) => ant && next, true);
   
    this.setState({
      allCheck: allCheck
    });
  }

  
  checkAll = () => {
    const checkItemState =this.state.checkItemState;
    const checkState = !this.state.allCheck;
    checkItemState.forEach((item, index) => checkItemState[index] = checkState);
    

    this.setState({
      checkItemState: checkItemState,
      allCheck: checkState
    });
  }
  

  render() {
    
    return (
    <HolderScroll > 
        {({headerPosition, firstColumnPosition}) => 
            <table>
                
                <DataTableHeader columns={this.props.columns} checked={this.state.allCheck} callback={this.checkAll} headerPosition={headerPosition} firstColumnPosition={firstColumnPosition}/>
                <tbody>
                    
                    { this.props.data.map((data, index) => 
                        <DataTableItem key={index} data={data} columns={this.props.columns} index={index} checked={this.state.checkItemState[index]} callback={this.checkRow} firstColumnPosition={firstColumnPosition}/>
                    )}     
                </tbody>               
            </table>
        }
    </HolderScroll>
    )
  }
}

DataTable.propTypes = {
    columns: PropTypes.arrayOf(PropTypes.shape({
        attribute: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
    })),
    data: PropTypes.arrayOf(PropTypes.object.isRequired),
}


