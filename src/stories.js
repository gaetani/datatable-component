import React from 'react';

import { storiesOf } from '@storybook/react';
import { DataTable } from './index';


const columns = [
    {
    "attribute": "field_name",
    "label": "Name"
    },
    {
    "attribute": "cultivation_id",
    "label": "Nummer"
    },
    {
    "attribute": "company_name",
    "label": "Betrieb"
    },
    {
    "attribute": "area_in_hectares",
    "label": "Fläche"
    },
    {
    "attribute": "active",
    "label": "Aktiv"
    },
    {
    "attribute": "crop_name",
    "label": "Kultur"
    },
    {
    "attribute": "harvest_year",
    "label": "Ernte"
    }
];
const data = 
[
	{
		"field_name": "Chester",
		"area_in_hectares": 89,
		"active": "false",
		"company_name": "Donec Non Justo Company",
		"cultivation_id": 9,
		"crop_name": 6,
		"harvest_year": 2009
	},
	{
		"field_name": "Honorato",
		"area_in_hectares": 42,
		"active": "false",
		"company_name": "Nulla Tempor Augue Consulting",
		"cultivation_id": 1,
		"crop_name": 1,
		"harvest_year": 2000
	},
	{
		"field_name": "Keaton",
		"area_in_hectares": 60,
		"active": "false",
		"company_name": "Euismod Et Commodo PC",
		"cultivation_id": 2,
		"crop_name": 5,
		"harvest_year": 2008
	},
	{
		"field_name": "Kadeem",
		"area_in_hectares": 29,
		"active": "false",
		"company_name": "Donec Corp.",
		"cultivation_id": 9,
		"crop_name": 9,
		"harvest_year": 1993
	},
	{
		"field_name": "Quentin",
		"area_in_hectares": 67,
		"active": "false",
		"company_name": "Sed Eu Limited",
		"cultivation_id": 7,
		"crop_name": 6,
		"harvest_year": 1990
	},
	{
		"field_name": "Ulysses",
		"area_in_hectares": 31,
		"active": "false",
		"company_name": "Cursus Company",
		"cultivation_id": 9,
		"crop_name": 3,
		"harvest_year": 1992
	},
	{
		"field_name": "Callum",
		"area_in_hectares": 87,
		"active": "false",
		"company_name": "Sit Amet Foundation",
		"cultivation_id": 4,
		"crop_name": 10,
		"harvest_year": 2005
	},
	{
		"field_name": "Alden",
		"area_in_hectares": 17,
		"active": "false",
		"company_name": "Velit LLP",
		"cultivation_id": 9,
		"crop_name": 7,
		"harvest_year": 2005
	},
	{
		"field_name": "Eric",
		"area_in_hectares": 46,
		"active": "false",
		"company_name": "Ac Limited",
		"cultivation_id": 6,
		"crop_name": 1,
		"harvest_year": 2003
	},
	{
		"field_name": "Jared",
		"area_in_hectares": 33,
		"active": "false",
		"company_name": "Ultricies Corp.",
		"cultivation_id": 4,
		"crop_name": 7,
		"harvest_year": 2009
	},
	{
		"field_name": "Mohammad",
		"area_in_hectares": 3,
		"active": "false",
		"company_name": "Auctor Company",
		"cultivation_id": 1,
		"crop_name": 1,
		"harvest_year": 1990
	},
	{
		"field_name": "Gannon",
		"area_in_hectares": 88,
		"active": "false",
		"company_name": "Fusce Dolor Quam Corporation",
		"cultivation_id": 7,
		"crop_name": 2,
		"harvest_year": 2009
	},
	{
		"field_name": "Carlos",
		"area_in_hectares": 3,
		"active": "false",
		"company_name": "Gravida Mauris Institute",
		"cultivation_id": 5,
		"crop_name": 6,
		"harvest_year": 1991
	},
	{
		"field_name": "Adrian",
		"area_in_hectares": 1,
		"active": "false",
		"company_name": "Purus In PC",
		"cultivation_id": 2,
		"crop_name": 9,
		"harvest_year": 2018
	},
	{
		"field_name": "Brandon",
		"area_in_hectares": 98,
		"active": "false",
		"company_name": "Tincidunt Pede LLC",
		"cultivation_id": 9,
		"crop_name": 6,
		"harvest_year": 1991
	},
	{
		"field_name": "Mason",
		"area_in_hectares": 56,
		"active": "false",
		"company_name": "At Risus Nunc PC",
		"cultivation_id": 10,
		"crop_name": 7,
		"harvest_year": 1993
	},
	{
		"field_name": "Dominic",
		"area_in_hectares": 63,
		"active": "false",
		"company_name": "Adipiscing Non Luctus LLC",
		"cultivation_id": 5,
		"crop_name": 9,
		"harvest_year": 2016
	},
	{
		"field_name": "Isaac",
		"area_in_hectares": 59,
		"active": "false",
		"company_name": "Tellus Consulting",
		"cultivation_id": 4,
		"crop_name": 9,
		"harvest_year": 2013
	},
	{
		"field_name": "Macaulay",
		"area_in_hectares": 37,
		"active": "false",
		"company_name": "Risus At Fringilla Industries",
		"cultivation_id": 7,
		"crop_name": 8,
		"harvest_year": 1997
	},
	{
		"field_name": "Giacomo",
		"area_in_hectares": 33,
		"active": "false",
		"company_name": "Mollis Corporation",
		"cultivation_id": 8,
		"crop_name": 2,
		"harvest_year": 2012
	},
	{
		"field_name": "Tucker",
		"area_in_hectares": 95,
		"active": "false",
		"company_name": "Et Libero Proin LLP",
		"cultivation_id": 7,
		"crop_name": 1,
		"harvest_year": 1992
	},
	{
		"field_name": "Chester",
		"area_in_hectares": 18,
		"active": "false",
		"company_name": "Ultrices Incorporated",
		"cultivation_id": 3,
		"crop_name": 8,
		"harvest_year": 1997
	},
	{
		"field_name": "Barry",
		"area_in_hectares": 76,
		"active": "false",
		"company_name": "Magna Sed Associates",
		"cultivation_id": 1,
		"crop_name": 5,
		"harvest_year": 2015
	},
	{
		"field_name": "Duncan",
		"area_in_hectares": 4,
		"active": "false",
		"company_name": "Etiam LLC",
		"cultivation_id": 6,
		"crop_name": 4,
		"harvest_year": 2005
	},
	{
		"field_name": "Conan",
		"area_in_hectares": 1,
		"active": "false",
		"company_name": "Consectetuer Adipiscing Institute",
		"cultivation_id": 3,
		"crop_name": 8,
		"harvest_year": 1991
	},
	{
		"field_name": "Wesley",
		"area_in_hectares": 55,
		"active": "false",
		"company_name": "Parturient Montes Corporation",
		"cultivation_id": 5,
		"crop_name": 1,
		"harvest_year": 1995
	},
	{
		"field_name": "Gregory",
		"area_in_hectares": 96,
		"active": "false",
		"company_name": "Neque Tellus Imperdiet Industries",
		"cultivation_id": 1,
		"crop_name": 8,
		"harvest_year": 2006
	},
	{
		"field_name": "Ross",
		"area_in_hectares": 80,
		"active": "false",
		"company_name": "Purus Institute",
		"cultivation_id": 10,
		"crop_name": 9,
		"harvest_year": 1998
	},
	{
		"field_name": "Ivan",
		"area_in_hectares": 33,
		"active": "false",
		"company_name": "Amet Metus Aliquam Incorporated",
		"cultivation_id": 1,
		"crop_name": 2,
		"harvest_year": 1998
	},
	{
		"field_name": "Lionel",
		"area_in_hectares": 69,
		"active": "false",
		"company_name": "Nisl Nulla Incorporated",
		"cultivation_id": 4,
		"crop_name": 7,
		"harvest_year": 2014
	},
	{
		"field_name": "Jamal",
		"area_in_hectares": 6,
		"active": "false",
		"company_name": "Arcu Morbi Sit Corp.",
		"cultivation_id": 6,
		"crop_name": 4,
		"harvest_year": 2001
	},
	{
		"field_name": "Caldwell",
		"area_in_hectares": 63,
		"active": "false",
		"company_name": "Et Pede Incorporated",
		"cultivation_id": 8,
		"crop_name": 9,
		"harvest_year": 2001
	},
	{
		"field_name": "Mason",
		"area_in_hectares": 99,
		"active": "false",
		"company_name": "Sit Amet Inc.",
		"cultivation_id": 8,
		"crop_name": 9,
		"harvest_year": 2012
	},
	{
		"field_name": "Uriel",
		"area_in_hectares": 15,
		"active": "false",
		"company_name": "Lorem Luctus Ut Incorporated",
		"cultivation_id": 9,
		"crop_name": 9,
		"harvest_year": 2007
	},
	{
		"field_name": "Eagan",
		"area_in_hectares": 71,
		"active": "false",
		"company_name": "Donec Tempor Est Company",
		"cultivation_id": 10,
		"crop_name": 9,
		"harvest_year": 2009
	},
	{
		"field_name": "Tyler",
		"area_in_hectares": 24,
		"active": "false",
		"company_name": "Curabitur Dictum Industries",
		"cultivation_id": 2,
		"crop_name": 8,
		"harvest_year": 2005
	},
	{
		"field_name": "Zachery",
		"area_in_hectares": 69,
		"active": "false",
		"company_name": "Cras Consulting",
		"cultivation_id": 4,
		"crop_name": 7,
		"harvest_year": 2012
	},
	{
		"field_name": "Zachery",
		"area_in_hectares": 96,
		"active": "false",
		"company_name": "Libero Morbi Foundation",
		"cultivation_id": 6,
		"crop_name": 2,
		"harvest_year": 2013
	},
	{
		"field_name": "Nolan",
		"area_in_hectares": 93,
		"active": "false",
		"company_name": "Posuere Cubilia Curae; Ltd",
		"cultivation_id": 5,
		"crop_name": 5,
		"harvest_year": 1994
	},
	{
		"field_name": "Akeem",
		"area_in_hectares": 98,
		"active": "false",
		"company_name": "Mauris LLP",
		"cultivation_id": 10,
		"crop_name": 8,
		"harvest_year": 2013
	},
	{
		"field_name": "Martin",
		"area_in_hectares": 36,
		"active": "false",
		"company_name": "Erat Volutpat Foundation",
		"cultivation_id": 2,
		"crop_name": 3,
		"harvest_year": 2000
	},
	{
		"field_name": "Herman",
		"area_in_hectares": 75,
		"active": "false",
		"company_name": "Id Inc.",
		"cultivation_id": 8,
		"crop_name": 8,
		"harvest_year": 1993
	},
	{
		"field_name": "Fitzgerald",
		"area_in_hectares": 4,
		"active": "false",
		"company_name": "Enim Mi Consulting",
		"cultivation_id": 4,
		"crop_name": 10,
		"harvest_year": 2009
	},
	{
		"field_name": "Hedley",
		"area_in_hectares": 70,
		"active": "false",
		"company_name": "Parturient Limited",
		"cultivation_id": 9,
		"crop_name": 8,
		"harvest_year": 2012
	},
	{
		"field_name": "Julian",
		"area_in_hectares": 4,
		"active": "false",
		"company_name": "In Faucibus Orci Corp.",
		"cultivation_id": 10,
		"crop_name": 6,
		"harvest_year": 2002
	},
	{
		"field_name": "Ezekiel",
		"area_in_hectares": 96,
		"active": "false",
		"company_name": "Velit Aliquam Nisl Company",
		"cultivation_id": 6,
		"crop_name": 9,
		"harvest_year": 2005
	},
	{
		"field_name": "Malachi",
		"area_in_hectares": 42,
		"active": "false",
		"company_name": "In Dolor LLC",
		"cultivation_id": 5,
		"crop_name": 2,
		"harvest_year": 2002
	},
	{
		"field_name": "Rudyard",
		"area_in_hectares": 12,
		"active": "false",
		"company_name": "Id Libero Inc.",
		"cultivation_id": 8,
		"crop_name": 6,
		"harvest_year": 2014
	},
	{
		"field_name": "Orson",
		"area_in_hectares": 16,
		"active": "false",
		"company_name": "A Consulting",
		"cultivation_id": 1,
		"crop_name": 1,
		"harvest_year": 1997
	},
	{
		"field_name": "Caleb",
		"area_in_hectares": 84,
		"active": "false",
		"company_name": "Eros Proin PC",
		"cultivation_id": 6,
		"crop_name": 3,
		"harvest_year": 1994
	},
	{
		"field_name": "Martin",
		"area_in_hectares": 1,
		"active": "false",
		"company_name": "Mauris Ltd",
		"cultivation_id": 6,
		"crop_name": 5,
		"harvest_year": 2000
	},
	{
		"field_name": "Daquan",
		"area_in_hectares": 57,
		"active": "false",
		"company_name": "Sed Id Risus LLC",
		"cultivation_id": 4,
		"crop_name": 9,
		"harvest_year": 1998
	},
	{
		"field_name": "Kirk",
		"area_in_hectares": 63,
		"active": "false",
		"company_name": "Eu Enim Consulting",
		"cultivation_id": 3,
		"crop_name": 2,
		"harvest_year": 1993
	},
	{
		"field_name": "Orson",
		"area_in_hectares": 84,
		"active": "false",
		"company_name": "Et LLC",
		"cultivation_id": 1,
		"crop_name": 2,
		"harvest_year": 2010
	},
	{
		"field_name": "Tucker",
		"area_in_hectares": 98,
		"active": "false",
		"company_name": "Massa Institute",
		"cultivation_id": 1,
		"crop_name": 9,
		"harvest_year": 2018
	},
	{
		"field_name": "Theodore",
		"area_in_hectares": 54,
		"active": "false",
		"company_name": "Aliquet Metus Limited",
		"cultivation_id": 10,
		"crop_name": 4,
		"harvest_year": 2006
	},
	{
		"field_name": "Boris",
		"area_in_hectares": 74,
		"active": "false",
		"company_name": "Egestas Urna Justo Associates",
		"cultivation_id": 6,
		"crop_name": 1,
		"harvest_year": 1990
	},
	{
		"field_name": "Nasim",
		"area_in_hectares": 30,
		"active": "false",
		"company_name": "Magna Ltd",
		"cultivation_id": 4,
		"crop_name": 6,
		"harvest_year": 2008
	},
	{
		"field_name": "Cole",
		"area_in_hectares": 100,
		"active": "false",
		"company_name": "Cubilia Curae; PC",
		"cultivation_id": 2,
		"crop_name": 7,
		"harvest_year": 2007
	},
	{
		"field_name": "Eagan",
		"area_in_hectares": 21,
		"active": "false",
		"company_name": "Donec Consulting",
		"cultivation_id": 2,
		"crop_name": 3,
		"harvest_year": 2006
	},
	{
		"field_name": "Buckminster",
		"area_in_hectares": 92,
		"active": "false",
		"company_name": "Pharetra LLC",
		"cultivation_id": 7,
		"crop_name": 4,
		"harvest_year": 2009
	},
	{
		"field_name": "Fitzgerald",
		"area_in_hectares": 10,
		"active": "false",
		"company_name": "Euismod Urna Nullam LLP",
		"cultivation_id": 1,
		"crop_name": 1,
		"harvest_year": 1995
	},
	{
		"field_name": "Oscar",
		"area_in_hectares": 14,
		"active": "false",
		"company_name": "Tellus Non Associates",
		"cultivation_id": 1,
		"crop_name": 6,
		"harvest_year": 1990
	},
	{
		"field_name": "Arsenio",
		"area_in_hectares": 95,
		"active": "false",
		"company_name": "Massa Quisque Porttitor Inc.",
		"cultivation_id": 2,
		"crop_name": 5,
		"harvest_year": 2014
	},
	{
		"field_name": "Wade",
		"area_in_hectares": 77,
		"active": "false",
		"company_name": "Enim Limited",
		"cultivation_id": 10,
		"crop_name": 3,
		"harvest_year": 1992
	},
	{
		"field_name": "Perry",
		"area_in_hectares": 45,
		"active": "false",
		"company_name": "Magna Suspendisse Tristique Foundation",
		"cultivation_id": 1,
		"crop_name": 7,
		"harvest_year": 1995
	},
	{
		"field_name": "Hu",
		"area_in_hectares": 47,
		"active": "false",
		"company_name": "Imperdiet Non LLC",
		"cultivation_id": 2,
		"crop_name": 1,
		"harvest_year": 2009
	},
	{
		"field_name": "Mason",
		"area_in_hectares": 72,
		"active": "false",
		"company_name": "Magna LLP",
		"cultivation_id": 9,
		"crop_name": 3,
		"harvest_year": 2008
	},
	{
		"field_name": "Leonard",
		"area_in_hectares": 30,
		"active": "false",
		"company_name": "Elit Elit Fermentum Consulting",
		"cultivation_id": 6,
		"crop_name": 4,
		"harvest_year": 1999
	},
	{
		"field_name": "Reed",
		"area_in_hectares": 42,
		"active": "false",
		"company_name": "Amet Incorporated",
		"cultivation_id": 1,
		"crop_name": 2,
		"harvest_year": 2013
	},
	{
		"field_name": "Jackson",
		"area_in_hectares": 17,
		"active": "false",
		"company_name": "Feugiat Institute",
		"cultivation_id": 4,
		"crop_name": 2,
		"harvest_year": 2011
	},
	{
		"field_name": "Hayden",
		"area_in_hectares": 39,
		"active": "false",
		"company_name": "Enim Nunc Ut Associates",
		"cultivation_id": 10,
		"crop_name": 10,
		"harvest_year": 1994
	},
	{
		"field_name": "Alvin",
		"area_in_hectares": 76,
		"active": "false",
		"company_name": "Ut Aliquam Associates",
		"cultivation_id": 3,
		"crop_name": 9,
		"harvest_year": 2009
	},
	{
		"field_name": "Denton",
		"area_in_hectares": 93,
		"active": "false",
		"company_name": "Curabitur Consulting",
		"cultivation_id": 10,
		"crop_name": 2,
		"harvest_year": 1994
	},
	{
		"field_name": "Solomon",
		"area_in_hectares": 1,
		"active": "false",
		"company_name": "Vestibulum Mauris LLP",
		"cultivation_id": 9,
		"crop_name": 10,
		"harvest_year": 2016
	},
	{
		"field_name": "Fletcher",
		"area_in_hectares": 62,
		"active": "false",
		"company_name": "Molestie Consulting",
		"cultivation_id": 3,
		"crop_name": 5,
		"harvest_year": 2009
	},
	{
		"field_name": "Geoffrey",
		"area_in_hectares": 67,
		"active": "false",
		"company_name": "Mauris Ut Quam Corporation",
		"cultivation_id": 2,
		"crop_name": 1,
		"harvest_year": 1998
	},
	{
		"field_name": "Robert",
		"area_in_hectares": 83,
		"active": "false",
		"company_name": "Mollis Foundation",
		"cultivation_id": 3,
		"crop_name": 3,
		"harvest_year": 1994
	},
	{
		"field_name": "Kamal",
		"area_in_hectares": 53,
		"active": "false",
		"company_name": "Sagittis Corp.",
		"cultivation_id": 1,
		"crop_name": 10,
		"harvest_year": 1996
	},
	{
		"field_name": "Hoyt",
		"area_in_hectares": 81,
		"active": "false",
		"company_name": "Egestas Lacinia PC",
		"cultivation_id": 10,
		"crop_name": 7,
		"harvest_year": 2015
	},
	{
		"field_name": "Hakeem",
		"area_in_hectares": 18,
		"active": "false",
		"company_name": "Neque PC",
		"cultivation_id": 4,
		"crop_name": 2,
		"harvest_year": 2009
	},
	{
		"field_name": "Dale",
		"area_in_hectares": 40,
		"active": "false",
		"company_name": "Ullamcorper Nisl Ltd",
		"cultivation_id": 10,
		"crop_name": 1,
		"harvest_year": 1996
	},
	{
		"field_name": "Griffith",
		"area_in_hectares": 85,
		"active": "false",
		"company_name": "Donec Nibh Quisque Corp.",
		"cultivation_id": 2,
		"crop_name": 10,
		"harvest_year": 2017
	},
	{
		"field_name": "Chadwick",
		"area_in_hectares": 77,
		"active": "false",
		"company_name": "A Scelerisque Sed Incorporated",
		"cultivation_id": 1,
		"crop_name": 2,
		"harvest_year": 1990
	},
	{
		"field_name": "Cade",
		"area_in_hectares": 91,
		"active": "false",
		"company_name": "Nonummy Ipsum Ltd",
		"cultivation_id": 3,
		"crop_name": 1,
		"harvest_year": 1995
	},
	{
		"field_name": "Beau",
		"area_in_hectares": 36,
		"active": "false",
		"company_name": "Nunc Commodo Auctor Ltd",
		"cultivation_id": 9,
		"crop_name": 5,
		"harvest_year": 2006
	},
	{
		"field_name": "Hunter",
		"area_in_hectares": 52,
		"active": "false",
		"company_name": "Proin Non Corp.",
		"cultivation_id": 4,
		"crop_name": 7,
		"harvest_year": 2015
	},
	{
		"field_name": "Tucker",
		"area_in_hectares": 60,
		"active": "false",
		"company_name": "Et Rutrum Non Company",
		"cultivation_id": 6,
		"crop_name": 9,
		"harvest_year": 2013
	},
	{
		"field_name": "Todd",
		"area_in_hectares": 24,
		"active": "false",
		"company_name": "Aliquam Company",
		"cultivation_id": 9,
		"crop_name": 3,
		"harvest_year": 2008
	},
	{
		"field_name": "Plato",
		"area_in_hectares": 62,
		"active": "false",
		"company_name": "Nam Foundation",
		"cultivation_id": 3,
		"crop_name": 9,
		"harvest_year": 2009
	},
	{
		"field_name": "Zeph",
		"area_in_hectares": 36,
		"active": "false",
		"company_name": "Enim Mi Tempor Limited",
		"cultivation_id": 3,
		"crop_name": 4,
		"harvest_year": 2012
	},
	{
		"field_name": "Aaron",
		"area_in_hectares": 66,
		"active": "false",
		"company_name": "Elit Ltd",
		"cultivation_id": 9,
		"crop_name": 3,
		"harvest_year": 2012
	},
	{
		"field_name": "Linus",
		"area_in_hectares": 29,
		"active": "false",
		"company_name": "Nullam Lobortis Associates",
		"cultivation_id": 5,
		"crop_name": 10,
		"harvest_year": 2011
	},
	{
		"field_name": "Jameson",
		"area_in_hectares": 96,
		"active": "false",
		"company_name": "Nibh Vulputate Mauris Limited",
		"cultivation_id": 5,
		"crop_name": 8,
		"harvest_year": 1991
	},
	{
		"field_name": "Curran",
		"area_in_hectares": 61,
		"active": "false",
		"company_name": "Lacus Pede Sagittis PC",
		"cultivation_id": 8,
		"crop_name": 2,
		"harvest_year": 2006
	},
	{
		"field_name": "Arden",
		"area_in_hectares": 66,
		"active": "false",
		"company_name": "Et Magnis Dis Company",
		"cultivation_id": 3,
		"crop_name": 2,
		"harvest_year": 1999
	},
	{
		"field_name": "Gray",
		"area_in_hectares": 73,
		"active": "false",
		"company_name": "Sed Nulla Incorporated",
		"cultivation_id": 4,
		"crop_name": 2,
		"harvest_year": 1996
	},
	{
		"field_name": "August",
		"area_in_hectares": 36,
		"active": "false",
		"company_name": "Eu Associates",
		"cultivation_id": 3,
		"crop_name": 8,
		"harvest_year": 2011
	},
	{
		"field_name": "Cyrus",
		"area_in_hectares": 6,
		"active": "false",
		"company_name": "Ligula Nullam Ltd",
		"cultivation_id": 1,
		"crop_name": 8,
		"harvest_year": 1995
	},
	{
		"field_name": "Steel",
		"area_in_hectares": 50,
		"active": "false",
		"company_name": "Hendrerit Industries",
		"cultivation_id": 10,
		"crop_name": 3,
		"harvest_year": 2007
	}
];

const Example = () => (
  <DataTable data={data} columns={columns}/>
);


storiesOf('DataTable', module)
  .add('simple example', () => <Example />);