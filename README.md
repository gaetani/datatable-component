## Synopsis

This is a coding challenge to Create a reusable react component for a dynamic DataTable. The requirements to complete the task are: 
- the header row should be fixed as the user scrolls vertical
- the first column should be fixed as the user scrolls horizontal
- the current row should be highlighted as it is hovered with a cursor (incl. the first column)

## Code Example

To use is very simple, after importing it, just type: 
<DataTable data={data} columns={columns} {....} />

If you want to see an running example: 
npm run storybook

## Motivation

To improve this dynamic table, I would like to add some features: 
- Auto formating of column, formating the column based on type, ie Date, Number, Currency. 
- Sorting 
- Paging 

## Installation

To install, just clone this repository and then : 
npm install 



## License

Apache